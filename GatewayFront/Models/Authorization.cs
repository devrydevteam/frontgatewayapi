﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class Authorization
    {
        public string MID { get; set; }

        public string MKEY { get; set; }

        public string POSTBACKURL { get; set; }

        public string SYSTEM { get; set; }

        public string RESPONSE { get; set; }

        public bool PARAMETER { get; set; } 
    }
}