﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class CieloTransactionResponse
    {
        public int HttpResponseStatusCode { get; set; }
        public string Intermediator { get; set; }
        public object Errors { get; set; }
        public Resultobject ResultObject { get; set; }
    }
}