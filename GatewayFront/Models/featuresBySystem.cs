﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class FeaturesBySystem
    {
        public string BankSlipEnabled { get; set; }
        public string CreditCardEnabled { get; set; }
        public string DebitCardEnabled { get; set; }
        public string HybridEnabled { get; set; }
    }
}