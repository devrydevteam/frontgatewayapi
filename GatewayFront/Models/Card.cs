﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class Card
    {
        public long CARDNUMBER { get; set; }

        public string HOLDER { get; set; }

        public string EXPIRATIONDATE { get; set; }

        public int SECURITYCODE { get; set; }

        public string BRAND { get; set; }
    }
}