﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class OrderDetails
    {
        public string DESCRIPTION { get; set; }

        public double VALUE { get; set; }
    }
}