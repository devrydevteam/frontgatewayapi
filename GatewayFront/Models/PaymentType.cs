﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class PaymentType
    {
        public string Brand { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}