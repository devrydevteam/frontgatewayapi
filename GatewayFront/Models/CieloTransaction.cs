﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GatewayFront.Models
{
    public class CieloTransaction
    {
        [Required]
        public string MerchantOrderId { get; set; }

        //[Required]
        //public List<OrderDetails> OrderDetails { get; set; }

        [Required]
        public Customer Customer { get; set; }

        [Required]
        public CieloPayment Payment { get; set; }
    }
}