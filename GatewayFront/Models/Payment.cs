﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class Payment
    {
        public int Amount { get; set; }
        public int Installments { get; set; }
        public string Interest { get; set; }
        public bool Capture { get; set; }
        public bool Authenticate { get; set; }
        public string SoftDescriptor { get; set; }
        public string Provider { get; set; }
        public int ServiceTaxAmount { get; set; }
        public string ProofOfSale { get; set; }
        public string Tid { get; set; }
        public string AuthorizationCode { get; set; }
        public string PaymentId { get; set; }
        public string Country { get; set; }
        public string AuthenticationUrl { get; set; }
        public int Status { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public int CapturedAmount { get; set; }
        public PaymentType PaymentType { get; set; }
    }
}