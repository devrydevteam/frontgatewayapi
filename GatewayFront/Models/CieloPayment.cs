﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class CieloPayment
    {
        public int Amount { get; set; }
        public int Installments { get; set; }
        public string SoftDescriptor { get; set; }
        public bool Capture { get; set; }
        public bool Authenticate { get; set; }
        public string ReturnUrl { get; set; }
        public CieloPaymentType PaymentType { get; set; }
    }
}