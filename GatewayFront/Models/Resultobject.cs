﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayFront.Models
{
    public class Resultobject
    {
        public string MerchantOrderId { get; set; }
        public Payment Payment { get; set; }
        public Customer Customer { get; set; }
    }
}