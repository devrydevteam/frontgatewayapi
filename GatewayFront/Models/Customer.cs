﻿
namespace GatewayFront.Models
{
    public class Customer
    {
        public string Name { get; set; }

        public string Identity { get; set; }

        public Address Address { get; set; }
    }
}