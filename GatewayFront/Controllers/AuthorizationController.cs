﻿using GatewayFront.Class;
using GatewayFront.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GatewayFront.Controllers
{
    public class AuthorizationController : Controller
    {
        // GET: Authorization
        [HttpGet]
        public ActionResult Index(Authorization authorization)
        {
            if (authorization != null)
            {
                ViewData["RESPONSE"] = authorization.RESPONSE;
                ViewData["POSTBACKURL"] = authorization.POSTBACKURL;
                ViewData["MID"] = authorization.MID;
                ViewData["SYSTEM"] = authorization.SYSTEM;
                if (!authorization.PARAMETER)
                    ViewData["EMPTYFIELDS"] = "O  formulário de requisição possui campos vazios";
                else
                    ViewData["EMPTYFIELDS"] = "A loja informada não possui autorização para transacionar";
            }
            else
                ViewBag.Erro = "Sua requisição está incorreta! Verifique a documentação do projeto para saber os campos obrigatórios";

            return View();
        }

        [HttpGet]
        public ActionResult PostBack(string urlPostBack)
        {
            return Redirect(urlPostBack);
        }

        public async Task<ActionResult> AppFormAsync(FormCollection form)
        {
            Authorization authorization = null;
            bool control = false;
            Session["cardToken"] = null;
            try
            {
                if (form.Count > 1)
                {
                    Session["sumPayment"] = 0;//this session control the partial amount in the hybrid flow
                    await GetStoreAuthorizationAsync(form["mId"], form["mKey"], form["ApplicationId"]);
                    string dataForm;
                    foreach (string _formData in form)
                    {
                        dataForm = form[_formData];
                        if (!string.IsNullOrEmpty(dataForm))
                            Session[_formData] = dataForm;
                        else
                            control = true;
                    }

                    if (Session["cardToken"] != null && !string.IsNullOrEmpty(Session["cardToken"].ToString()))
                        if (!string.IsNullOrEmpty(form["cardToken"].ToString()) && !control)
                            return RedirectToAction("ByOneClick", "CieloSales");

                    if (control)
                    {
                        authorization = new Authorization
                        {
                            MID = form["mId"],
                            RESPONSE = "forbiden",
                            POSTBACKURL = form["postBackUrl"],
                            SYSTEM = form["system"],
                            PARAMETER = false//when the form has empty values
                        };
                        return RedirectToAction("Index", "Authorization", authorization);
                    }
                }
                else if (Session["hybridPayment"] != null && Session["hybridPayment"].ToString().Equals("true"))
                {
                    List<Payment> successList = (List<Payment>)Session["lPayment"];
                    List<Payment> generalList = (Session["generalList"]) == null ? new List<Payment>() : (List<Payment>)Session["generalList"];

                    for (int i = 0; i < successList.Count; i++)
                    {
                        if (successList[i].Status != 2 && generalList.Exists(x => x.PaymentId == successList[i].PaymentId))
                            successList.Remove(successList[i]);
                        if (!generalList.Exists(x => x.PaymentId == successList[i].PaymentId))
                            generalList.Add(successList[i]);
                    }

                    Session["generalList"] = generalList;
                    ViewBag.Payments = successList;
                }
            }
            catch (System.Exception)
            {
                throw;
            }

            return View();
        }

        private async Task<FeaturesBySystem> GetStoreAuthorizationAsync(string merchantKey, string merchantId, string app)
        {
            var response = await new AuthorizationCieloProxy(merchantId, merchantKey, app).GetAsync();
            FeaturesBySystem authorizationResponse = null;
            if (response != null)
                authorizationResponse = (FeaturesBySystem)response;
            Session["BankSlipEnabled"] = authorizationResponse.BankSlipEnabled;
            Session["HybridEnabled"] = authorizationResponse.HybridEnabled;
            return authorizationResponse;
        }
    }
}