﻿using GatewayFront.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using GatewayFront.Class;
using GatewayFront.ConstTypes;

namespace GatewayFront.Controllers
{
    public class CieloSalesController : Controller
    {
        private List<Payment> lPayment = null;
        
        // GET: CieloSales
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SaleDebit()
        {
            try
            {
                CieloTransactionProxy cieloTransactionProxy = new CieloTransactionProxy(Session["mKey"].ToString(), Session["mId"].ToString(), Session["ApplicationId"].ToString(), Session["paymentId"].ToString());
                CieloTransactionResponse cieloTransactionResponse = (CieloTransactionResponse)cieloTransactionProxy.Get();
                ViewData["postBackUrl"] = Session["postBackUrl"].ToString();

                if (cieloTransactionResponse != null)
                {
                    if (cieloTransactionResponse.ResultObject.Payment.Status == 2)
                        ViewData["Success"] = "true";
                    else
                        ViewData["Success"] = "false";

                    ViewData["orderId"] = cieloTransactionResponse.ResultObject.MerchantOrderId;
                    ViewData["tid"] = cieloTransactionResponse.ResultObject.Payment.Tid;
                    ViewData["message"] = cieloTransactionResponse.ResultObject.Payment.ReturnMessage;

                    if (Session["hybridPayment"].ToString().Equals("true"))
                        return RedirectToAction("HybridPayment", "CieloSales", cieloTransactionResponse.ResultObject.Payment);
                }
                ViewData["Success"] = "false";
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        public ActionResult ByOneClick()
        {
            if (!string.IsNullOrEmpty(Session["cardToken"].ToString()))
                ViewData["cardToken"] = Session["cardToken"].ToString();

            return View();
        }

        public ActionResult Sale(FormCollection form)
        {
            try
            {
                if (form.Count > 1)
                {
                    bool hybridPayment = (Session["hybridPayment"] == null) ? false : true;
                    bool bankSlip = false;
                    bool capture = false;
                    string year = null, month = null;

                    if (string.IsNullOrEmpty(form["cardToken"]))
                    {
                        if (string.IsNullOrEmpty(form["bankSlip"]))
                        {
                            year = form["ExpirationDate"].ToString().Substring(0, 4);
                            month = form["ExpirationDate"].ToString().Substring(5, 2);
                            if (form["radiobtn"].ToString().Equals("creditValue"))
                                capture = true;
                        }
                        else bankSlip = true;
                    }
                    else capture = true;

                    //List<OrderDetails> listOrderdetails = new List<OrderDetails>();
                    double totalValue;
                    if(!string.IsNullOrEmpty(form["partialAmount"]))
                        totalValue = Convert.ToDouble(form["partialAmount"].ToString());
                    else
                        totalValue = Convert.ToDouble(form["amount"]) - Convert.ToDouble(Session["sumPayment"].ToString());
                    OrderDetails orderDetails = new OrderDetails
                    {
                        DESCRIPTION = form["description"],
                        VALUE = totalValue
                    };
                    //listOrderdetails.Add(orderDetails);
                    Customer customer = new Customer
                    {
                        Name = string.IsNullOrEmpty(form["holder"]) ? form["name"] : form["holder"]
                    };
                    CieloPaymentType paymentType;
                    if (string.IsNullOrEmpty(form["cardToken"]) && !bankSlip)
                    {
                        paymentType = new CieloPaymentType
                        {
                            CardNumber = form["cardNumber"],
                            Holder = form["holder"],
                            ExpirationDate = month + "/" + year,
                            SecurityCode = form["cvv"],
                            Type = capture ? ConstOperations.Credit : ConstOperations.Debit
                        };
                    }
                    else if (!bankSlip)
                    {
                        paymentType = new CieloPaymentType
                        {
                            CardToken = form["cardToken"],
                            SecurityCode = form["cvv"],
                            Type = ConstOperations.Credit,
                            Brand = form["brand"]
                        };
                    }
                    else
                    {
                        customer.Identity = form["identity"];
                        paymentType = new CieloPaymentType
                        {
                            Type = ConstOperations.BankSlip
                        };
                        CepResponse cepResponse = (CepResponse)new CepProxy(form["zipCode"]).Get();
                        Address address = new Address
                        {
                            City = cepResponse.Localidade,
                            Complement = string.IsNullOrEmpty(cepResponse.Complemento) ? "Complemento: " : cepResponse.Complemento,
                            Country = "Br",
                            District = cepResponse.Bairro,
                            Number = string.IsNullOrEmpty(cepResponse.Gia) ? "S/N" : cepResponse.Gia,
                            State = cepResponse.Uf,
                            Street = string.IsNullOrEmpty(cepResponse.Logradouro) ? cepResponse.Localidade : cepResponse.Logradouro,
                            ZipCode = form["zipCode"]
                        };
                        customer.Address = address;
                    }

                    CieloPayment payment = new CieloPayment
                    {
                        PaymentType = paymentType,
                        Amount = Convert.ToInt32(orderDetails.VALUE * 100),
                        Installments = capture ? Convert.ToInt32(form["installments"]) : 0,
                        SoftDescriptor = "AdTalem",
                        Capture = capture,
                        Authenticate = capture ? false : true,
                        ReturnUrl = capture ? "" : System.Web.Configuration.WebConfigurationManager.AppSettings["DebitPage"]
                    };
                    CieloTransaction cieloTransaction = new CieloTransaction
                    {
                        MerchantOrderId = form["orderId"],
                        Customer = customer,
                        Payment = payment
                    };

                    CieloTransactionProxy cieloTransactionProxy = new CieloTransactionProxy(form["mKey"], form["mId"], form["ApplicationId"], cieloTransaction);
                    CieloTransactionResponse response = (CieloTransactionResponse)cieloTransactionProxy.Post();
                    ViewData["postBackUrl"] = form["postBackUrl"];
                    if (response != null)
                        if (response.HttpResponseStatusCode.Equals(201))
                        {
                            #region commentHybridflow
                            /*
                             * hybrid payments allow users combine two or more kind of payment 
                             * the flow is controled by session hybridpayment, whether this session has true value, the hybrid flow is invoked
                             * partialAmount -> it's the value digited by an user on the field value
                             */
                            #endregion
                            if (!hybridPayment && !string.IsNullOrEmpty(form["partialAmount"]) && Convert.ToDouble(Session["sumPayment"]) < Convert.ToDouble(Session["amount"]))
                            {
                                hybridPayment = true;
                                Session["hybridPayment"] = "true";
                                Session["sumPayment"] = 0;
                            }
                            if (bankSlip && response.ResultObject.Payment.Status == 1)
                            {
                                return Redirect(response.ResultObject.Payment.PaymentType.Url);
                            }
                            if (response.ResultObject.Payment.Status == 2)
                            {
                                ViewData["success"] = "true";
                                ViewData["authorizationCode"] = response.ResultObject.Payment.AuthorizationCode;
                                ViewData["tid"] = response.ResultObject.Payment.Tid;
                                ViewData["mId"] = form["mId"];
                                ViewData["paymentId"] = response.ResultObject.Payment.PaymentId;
                                ViewData["message"] = response.ResultObject.Payment.ReturnMessage;
                                ViewData["orderId"] = response.ResultObject.MerchantOrderId;
                            }
                            else if (response.ResultObject.Payment.Status == 0 && response.ResultObject.Payment.PaymentType.Type.Equals(ConstOperations.Debit))
                            {
                                Session["mId"] = form["mId"];
                                Session["mKey"] = form["mKey"];
                                Session["postBackUrl"] = form["postBackUrl"];
                                Session["paymentId"] = response.ResultObject.Payment.PaymentId;
                                return Redirect(response.ResultObject.Payment.AuthenticationUrl);
                            }
                            else ViewData["success"] = "false";

                            if (hybridPayment)
                                return RedirectToAction("HybridPayment", "CieloSales", response.ResultObject.Payment);
                        }
                        else
                        {
                            ViewData["error"] = "Transação sem retorno";
                            ViewData["success"] = "false";
                        }
                    else
                    {
                        ViewData["error"] = "Transação sem retorno";
                        ViewData["success"] = "false";
                    }
                }
            }
            catch (System.Exception)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        public ActionResult HybridPayment(Payment payment)
        {
            lPayment = (Session["lPayment"] == null) ? new List<Payment>() : (List<Payment>)Session["lPayment"];
            lPayment.Add(payment);

            if (payment.Status == 2 && Session["sumPayment"] != null)
                Session["sumPayment"] = payment.Amount + Convert.ToDouble(Session["sumPayment"]);

            Session["lPayment"] = lPayment;

            return RedirectToAction("AppFormAsync", "Authorization");
        }
    }
}