﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GatewayFront.Class
{
    public static class RestCall
    {
        public static string Get(string uri, Dictionary<string, string> header = null, string parameter = null)
        {
            string Json = null;

            try
            {
                if (!string.IsNullOrEmpty(parameter))
                    uri += "/" + parameter;

                WebClient webClient = new WebClient();
                webClient.Headers.Add("Content-Type", "application/json");
                if (header != null)
                    foreach (KeyValuePair<string, string> key in header)
                        webClient.Headers.Add(key.Key, key.Value);

                byte[] data = webClient.DownloadData(uri);
                
                if(data.Length > 1)
                    Json = Encoding.UTF8.GetString(data);

                return Json;
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
                return Json;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string Post(Dictionary<string, string> header, string uri, string payload)
        {
            string json = null;
            
            try
            {
                WebClient webClient = new WebClient();
                webClient.Headers.Add("Content-Type", "application/json");
                foreach (KeyValuePair<string, string> key in header)
                    webClient.Headers.Add(key.Key, key.Value);

                byte[] data = webClient.UploadData(uri, "POST", Encoding.UTF8.GetBytes(payload));

                if (data.Length > 0)
                    json = Encoding.UTF8.GetString(data);

                return json;
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static async Task<string> GetAsync(Dictionary<string, string> header, string uri, string parameter = null)
        {
            string response = null;
            try
            {
                if (string.IsNullOrEmpty(parameter))
                    uri += parameter;
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    foreach (KeyValuePair<string, string> key in header)
                        client.DefaultRequestHeaders.Add(key.Key, key.Value);
                    using (HttpResponseMessage message = await client.GetAsync(uri))
                    {
                        using (HttpContent content = message.Content)
                        {
                            response = await content.ReadAsStringAsync();
                        }
                    }
                }
                return response;
            }
            catch (System.Exception ex)
            {
                string cv = ex.InnerException.ToString();
                return response;
            }
        }
    }
}