﻿using GatewayFront.Models;
using System;
using Newtonsoft.Json.Linq;


namespace GatewayFront.Class
{
    public class CepProxy : IRestInterface
    {
        private readonly string url = System.Configuration.ConfigurationManager.AppSettings["cep"];
        private string zipCode;

        public CepProxy(string zipCode)
        {
            this.zipCode = zipCode;
        }

        public object Get()
        {
            CepResponse cepResponse = null;
            string uri = url + zipCode + "/json/";
            string response = RestCall.Get(uri);
            if (response.Length > 0)
                cepResponse = JObject.Parse(response).ToObject<CepResponse>();

            return cepResponse;
            throw new NotImplementedException();
        }

        public object Post()
        {
            throw new NotImplementedException();
        }
    }
}