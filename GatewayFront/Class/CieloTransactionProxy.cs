﻿using System;
using System.Collections.Generic;
using GatewayFront.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GatewayFront.Class
{
    public class CieloTransactionProxy : IRestInterface
    {
        private readonly string ulr = System.Configuration.ConfigurationManager.AppSettings["GatewayAPI"] + "/sales";
        private Dictionary<string, string> header = null;
        private CieloTransaction transaction = null;
        private string parameter;

        public CieloTransactionProxy(string mKey, string mId, string app, CieloTransaction transaction)
        {
            this.header = new Dictionary<string, string>();
            header.Add("merchantId", mId);
            header.Add("merchantKey", mKey);
            header.Add("ApplicationId", app);
            this.transaction = transaction;
        }

        public CieloTransactionProxy(string mKey, string mId, string app, string parameter)
        {
            this.header = new Dictionary<string, string>
            {
                {"MerchantId", mId },
                {"MerchantKey", mKey },
                {"ApplicationId", app }
            };
            this.parameter = parameter;
        }

        public object Get()
        {
            CieloTransactionResponse transactionResponse = null;
            try
            {                
                string response = RestCall.Get(this.ulr, this.header, this.parameter);
                if (response.Length > 0)
                    return transactionResponse = JObject.Parse(response).ToObject<CieloTransactionResponse>();

                return transactionResponse;
            }
            catch (Exception)
            {
                return transactionResponse;
            }
            throw new NotImplementedException();
        }

        public object Post()
        {
            CieloTransactionResponse transactionResponse = null;
            try
            {
                string response = RestCall.Post(this.header, this.ulr, JsonConvert.SerializeObject(transaction));
                if (response.Length > 0)
                    return transactionResponse = JObject.Parse(response).ToObject<CieloTransactionResponse>();

                return transactionResponse;
            }
            catch (Exception)
            {
                return transactionResponse;
            }
            throw new NotImplementedException();

        }
    }
}