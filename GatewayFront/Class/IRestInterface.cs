﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GatewayFront.Class
{
    interface IRestInterface
    {
        Object Get();
        Object Post();
    }
}
