﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using GatewayFront.Models;
using System.Threading.Tasks;

namespace GatewayFront.Class
{
    public class AuthorizationCieloProxy : IRestInterface
    {
        private readonly string url = System.Configuration.ConfigurationManager.AppSettings["GATEWAYAPI"] + "/Permissions";
        private Dictionary<string, string> header = null;

        public AuthorizationCieloProxy (string mkey, string mId, string app)
        {
            header = new Dictionary<string, string>
            {
                { "MerchantId", mId },
                { "MerchantKey", mkey },
                { "ApplicationId", app}
            };
        }

        public async Task<Object> GetAsync()
        {
            FeaturesBySystem authorizationResponse = null;
            try
            {
                string response = await RestCall.GetAsync(header, url);
                //if (JObject.Parse("status").SelectToken(response).ToString().Equals("200"))
                if (response.Length > 0)
                    authorizationResponse = JObject.Parse(response).ToObject<FeaturesBySystem>();


                return authorizationResponse;
            }
            catch (Exception)
            {
                return authorizationResponse;
            }
        }

        public object Get()
        {
            Authorization authorization = null;
            try
            {
                string response = RestCall.Get(url, header);
                if (JObject.Parse("status").SelectToken(response).ToString().Equals("200"))
                    authorization = JObject.Parse(response).SelectToken("Authorization").ToObject<Authorization>();


                return authorization;
            }
            catch (Exception)
            {
                return authorization;
            }
            throw new NotImplementedException();
        }

        public object Post()
        {
            throw new NotImplementedException();
        }
    }
}