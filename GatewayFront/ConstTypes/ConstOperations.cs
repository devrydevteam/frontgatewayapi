﻿namespace GatewayFront.ConstTypes
{
    public static class ConstOperations
    {
        public const string Credit = "CreditCard";
        public const string Debit = "DebitCard";
        public const string ByOneClick = "ByOneClick";
        public const string BankSlip = "BankSlip";
    }
}